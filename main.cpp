#include <cstdio>
#include <queue>
using namespace std;

struct sNode;
typedef struct sNode Node;
struct sNode{
	Node* next;
	int v; // the degree or the vertex number
};

Node adj[200];

int t, n, m, k;

// DFS search
// tag_vertex: the vertex found previously
// return 0 if found a solution
// return -1 if not found
int dfs_search(Node *adj, int n, int k, queue<int>& tag_vertex);
void initialize(Node *adj, int n);
void add_edge(Node *adj, int x, int y);
Node *find_end(Node *adj, int v);
void print_graph(Node *adj, int n);
void del_vertex(Node *adj, int v);
void del_edge(Node *adj, int x, int y);
// get the vertex number of degree 1
// return the vertex number if found
// return negative if not found
// return -1*v where v is the maximum 
// degree vertex number
// return 0 if the graph is empty
int get_deg1(Node *adj, int n); 
// delte graph
// adj: adj list
// n: node number
void del_graph(Node *adj, int n);
Node *copy_graph(Node *adj, int n);

int main(void) {
	freopen("input", "r", stdin);
	scanf("%d", &t);
	for(int test_num = 0; test_num < t; test_num++ ) {
		scanf("%d%d%d", &n, &m, &k);
		initialize(adj, n);
		for(int i = 0; i < m; i++ ) {
			int x, y;
			scanf("%d%d", &x, &y);
			add_edge(adj, x, y);
		}
		queue<int> tag_vertex;
		int ret = dfs_search(adj, n, k, tag_vertex);
		if(ret == -1)
			printf("-1\n");
		else {
			printf("%d\n", tag_vertex.size());
			while(!tag_vertex.empty()) {
				printf("%d ", tag_vertex.front());
				tag_vertex.pop();
			}
			printf("\n");
		}
	}
}

int dfs_search(Node *adj, int n, int k, queue<int>& tag_vertex) {
	// copy graph
	Node *new_adj = copy_graph(adj, n);
	queue<int> son_vertex;
	// choose a vertex and prune
	int cho_v = -1;
	while((cho_v = get_deg1(new_adj, n)) > 0) {
		int del_v = new_adj[cho_v].next->v;
		son_vertex.push(del_v);
		del_vertex(new_adj, del_v);
		k--;
	}
	if (k<0 || (k == 0 && cho_v < 0)) {
		del_graph(new_adj, n);
		return -1;
	}
	if (cho_v == 0) {
		del_graph(new_adj, n);
		while(!son_vertex.empty()) {
			tag_vertex.push(son_vertex.front());
			son_vertex.pop();
		}
		return 0;
	}
	Node *bak_adj = copy_graph(new_adj, n);
	// search left
	cho_v = -1*cho_v;
	son_vertex.push(cho_v);
	del_vertex(new_adj, cho_v);
	k--;
	int ret = dfs_search(new_adj, n, k, son_vertex);
	if(ret == 0) {
		while(!son_vertex.empty()) {
			tag_vertex.push(son_vertex.front());
			son_vertex.pop();
		}
		del_graph(new_adj, n);
		del_graph(bak_adj, n);
		return 0;
	} else {
		//  search right
		son_vertex.pop();
		k++;
		del_graph(new_adj, n);
		new_adj = bak_adj;
		// delete the adj vertexes
		Node *node = new_adj[cho_v].next;
		while(node != NULL) {
			int bak_v = node->v;
			node = node->next;
			son_vertex.push(bak_v);
			del_vertex(new_adj, bak_v);
			k--;
		}
		if (k < 0) {
			del_graph(new_adj, n);
			return -1;
		} else {
			ret = dfs_search(new_adj, n, k, son_vertex);
			if (ret == 0) {
				while(!son_vertex.empty()) {
					tag_vertex.push(son_vertex.front());
					son_vertex.pop();
				}
				del_graph(new_adj, n);
				return 0;
			} else {
				del_graph(new_adj, n);
				return -1;
			}
		}
	}
}

void initialize(Node *adj, int n) {
	if (adj == NULL)
		adj = new Node[n+1];
	for (int i = 0; i <= n; i++) {
		adj[i].next = NULL;
		adj[i].v = 0;
	}
}

void add_edge(Node *adj, int x, int y) {
	if (x == y)
		return ;
	Node *node = new Node;
	node->v = y;
	node->next = adj[x].next;
	adj[x].next = node;
	adj[x].v ++;
	node = new Node;
	node->v = x;
	node->next = adj[y].next;
	adj[y].next = node;
	adj[y].v ++;
}

Node *find_end(Node *adj, int v) {
	Node *node = &adj[v];
	while(node->next != NULL) {
		node = node->next;
	}
	return node;
}

void print_graph(Node *adj, int n) {
	for(int i = 1; i <= n; i++) {
		printf("vertex %d(%d): ", i, adj[i].v);
		Node *node = adj[i].next;
		while(node != NULL) {
			printf(" %d", node->v);
			node = node->next;
		}
		printf("\n");
	}
	printf("\n\n");
}

void del_vertex(Node *adj, int v) {
	Node *node = adj[v].next;
	while(node != NULL) {
		int tempv = node->v;
		node = node->next;
		del_edge(adj, v, tempv);
	}
}

void del_edge(Node *adj, int x, int y) {
	Node *node = adj[x].next;
	Node *prev = &adj[x];
	while(node != NULL && node->v != y) {
		prev = node;
		node = node->next;
	}
	if(node != NULL && node->v == y) {
		prev->next = node->next;
		delete node;
		adj[x].v--;
	}
	node = adj[y].next;
	prev = &adj[y];
	while(node != NULL && node->v != x) {
		prev = node;
		node = node->next;
	}
	if(node != NULL && node->v == x) {
		prev->next = node->next;
		delete node;
		adj[y].v--;
	}
}
// to be optimized
int get_deg1(Node *adj, int n) {
	int max_v = 0;
	for( int i = 1; i <= n; i++) {
		if(adj[i].v == 1) {
			return i;
		} else if (adj[i].v > adj[max_v].v) {
			max_v = i;
		}
	}
	return -1 * max_v;
}

void del_graph(Node *adj, int n) {
	if(adj == NULL)
		return;
	for( int i = 0; i <= n; i++) {
		Node *node = adj[i].next;
		while( node != NULL ) {
			Node *next = node->next;
			// printf("delete node %d:%d\n", i, node->v);
			delete node;
			node = next;
		}
		adj[i].next = NULL;
	}
	delete []adj;
	adj = NULL;
}

Node *copy_graph(Node *adj, int n) {
	Node *new_adj = new Node[n+1];
	for(int i = 0; i <= n; i++) {
		new_adj[i].v = adj[i].v;
		Node *new_node = &new_adj[i];
		Node *node = adj[i].next;
		while(node != NULL) {
			new_node->next = new Node;
			new_node = new_node->next;
			new_node->v = node->v;
			node = node->next;
		}
		new_node->next = NULL;
	}
	return new_adj;
}
